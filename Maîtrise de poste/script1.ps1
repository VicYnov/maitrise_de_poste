# Affiche le nom de l'h�te
Write-host "Nom du poste : $env:COMPUTERNAME"


# IP principale
"IP principale : "
Get-NetIPAddress -InterfaceIndex 11 | Format-Table

# Nom et version de l'OS 
Get-WmiObject Win32_OperatingSystem | Select-Object Caption, Version

# Heure et date du dernier boot
Get-CimInstance -ClassName win32_operatingsystem | select csname, lastbootuptime

# RAM

echo "Espace de la RAM utilis�"

echo "Espace libre de la RAM : $(Get-CimInstance Win32_PhysicalMemory | Measure-Object -Property capacity -Sum | Foreach {"{0:N2}" -f ([math]::round(($_.Sum / 1GB),2))})"

# Disque

echo "Espace disque utilise : "
gwmi win32_logicaldisk | Format-Table DeviceId, @{n="Size";e={[math]::Round($_.Size/1GB,2)}},@{n="UsedSpace";e={[math]::Round(($_.Size-$_.FreeSpace)/1GB,2)}}

echo "Espace disque dispo : "
gwmi win32_logicaldisk | Format-Table DeviceId, @{n="Size";e={[math]::Round($_.Size/1GB,2)}},@{n="FreeSpace";e={[math]::Round($_.FreeSpace/1GB,2)}}

# Utilisateurs

echo "Liste des utilisateurs : "
wmic useraccount list full

# On fait le ping

ping 8.8.8.8