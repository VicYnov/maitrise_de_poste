# Maîtrise de poste - Day 1

## Self-footprinting

### Host OS

Informations sur ma machine : 

Avec la commande ```systeminfo```, on obtient plein d'info diverses et variées...j'ai supprimé quelques lignes pour garder ce qui nous intéresse

```

PS C:\Users\victo> systeminfo

Nom de l’hôte:                              LAPTOP-SI7QQC7H
Nom du système d’exploitation:              Microsoft Windows 10 Famille
Version du système:                         10.0.18362 N/A version 18362
Processeur(s):                              1 processeur(s) installé(s).
                                            [01] : Intel64 Family 6 Model 126 Stepping 5 GenuineIntel ~1298 MHz
Type du système:                            x64-based PC
Mémoire physique totale:                    7 964 Mo
```

J'ai trouvé une commande pour afficher toutes les infos de mon processeur ```wmic cpu get``` mais celle ci ne s'affiche pas correctement dans mes terminaux...

```
PS C:\Users\victo> wmic cpu get
AddressWidth  Architecture  AssetTag                Availability  Caption                                 Characteristics  ConfigManagerErrorCode  ConfigManagerUserConfig  CpuStatus  CreationClassName  CurrentClockSpeed  CurrentVoltage  DataWidth  Description                             DeviceID  ErrorCleared  ErrorDescription  ExtClock  Family  InstallDate  L2CacheSize  L2CacheSpeed  L3CacheSize  L3CacheSpeed  LastErrorCode  Level  LoadPercentage  Manufacturer  MaxClockSpeed  Name                                      NumberOfCores  NumberOfEnabledCore  NumberOfLogicalProcessors  OtherFamilyDescription  PartNumber              PNPDeviceID  PowerManagementCapabilities  PowerManagementSupported  ProcessorId       ProcessorType  Revision  Role  SecondLevelAddressTranslationExtensions  SerialNumber            SocketDesignation  Status  StatusInfo  Stepping  SystemCreationClassName  SystemName       ThreadCount  UniqueId  UpgradeMethod  Version  VirtualizationFirmwareEnabled  VMMonitorModeExtensions  VoltageCaps
64            9             To Be Filled By O.E.M.  3             Intel64 Family 6 Model 158 Stepping 12  236                                                               1          Win32_Processor    3696               10              64         Intel64 Family 6 Model 158 Stepping 12  CPU0                                      100       205                  1536                       9216         0                            6      17              GenuineIntel  3696           Intel(R) Core(TM) i5-9600K CPU @ 3.70GHz  6              6                    6                                                  To Be Filled By O.E.M.                                            FALSE                     BFEBFBFF000906EC  3                        CPU   TRUE                                     To Be Filled By O.E.M.  U3E1               OK      3                     Win32_ComputerSystem     DESKTOP-B962H64  6                      50                      TRUE                           TRUE
```

Information sur le modèle de la ram : 

```
C:\Users\victo>wmic memorychip get devicelocator, manufacturer
DeviceLocator   Manufacturer
ChannelA-DIMM0  Samsung
ChannelB-DIMM0  Samsung
```

### Devices

Informations sur le processeur : 

```

PS C:\Users\victo> Get-ComputerInfo -Property "*proc*"


CsNumberOfLogicalProcessors : 8
CsNumberOfProcessors        : 1
CsProcessors                : {Intel(R) Core(TM) i7-1065G7 CPU @ 1.30GHz}
OsMaxNumberOfProcesses      : 4294967295
OsMaxProcessMemorySize      : 137438953344
OsNumberOfProcesses         : 218

```

Explication du nom du processeur Intel : 

Le premier chiffre après le tiret correspond à la génération du processeur : ici il s'agit de la 10ème génération de processeur Intel. Les chiffres suivant servent de référence au fabriquant. Enfin G7 correspond au niveau de performances graphiques du processeur.

Partition du disque dur : 

```
DISKPART> list partition

  N° partition   Type              Taille   Décalage
  -------------  ----------------  -------  --------
  Partition 1    Système            100 M   1024 K
  Partition 2    Réservé             16 M    101 M
  Partition 3    Principale         475 G    117 M
  Partition 4    Récupération      1024 M    475 G
```

La partition 1 sert à héberger le système d'exploitation
La partition 2 est réservé, mais je sais pas pourquoi ;(
La partition 3 sert à stocker toutes ce qu'on enregistre, télécharge et installe sur le pc
La partition 4 sert à héberger les sauvegardes, que l'on peut récuperer en cas de défaillance de la machine

### Network

Liste de toutes les cartes réseau : 

```
PS C:\Users\victo> ipconfig /all

Configuration IP de Windows

   Nom de l’hôte . . . . . . . . . . : LAPTOP-SI7QQC7H
   Suffixe DNS principal . . . . . . :
   Type de noeud. . . . . . . . . .  : Hybride
   Routage IP activé . . . . . . . . : Non
   Proxy WINS activé . . . . . . . . : Non
   Liste de recherche du suffixe DNS.: home

Carte Ethernet VirtualBox Host-Only Network :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : VirtualBox Host-Only Ethernet Adapter
   Adresse physique . . . . . . . . . . . : 0A-00-27-00-00-13
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::489d:d189:7ecf:9143%19(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.3.2.1(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . :
   IAID DHCPv6 . . . . . . . . . . . : 654966823
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-25-3D-21-5D-00-6F-00-00-32-5E
   Serveurs DNS. . .  . . . . . . . . . . : fec0:0:0:ffff::1%1
                                       fec0:0:0:ffff::2%1
                                       fec0:0:0:ffff::3%1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

Carte Ethernet VirtualBox Host-Only Network #2 :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : VirtualBox Host-Only Ethernet Adapter #2
   Adresse physique . . . . . . . . . . . : 0A-00-27-00-00-12
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::cc2:ed09:3b60:d5c5%18(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.3.1.1(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . :
   IAID DHCPv6 . . . . . . . . . . . : 856293415
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-25-3D-21-5D-00-6F-00-00-32-5E
   Serveurs DNS. . .  . . . . . . . . . . : fec0:0:0:ffff::1%1
                                       fec0:0:0:ffff::2%1
                                       fec0:0:0:ffff::3%1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

Carte Ethernet VirtualBox Host-Only Network #3 :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : VirtualBox Host-Only Ethernet Adapter #3
   Adresse physique . . . . . . . . . . . : 0A-00-27-00-00-0A
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::501a:92:ffec:da43%10(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.4.1.1(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . :
   IAID DHCPv6 . . . . . . . . . . . : 923402279
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-25-3D-21-5D-00-6F-00-00-32-5E
   Serveurs DNS. . .  . . . . . . . . . . : fec0:0:0:ffff::1%1
                                       fec0:0:0:ffff::2%1
                                       fec0:0:0:ffff::3%1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

Carte Ethernet VirtualBox Host-Only Network #4 :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : VirtualBox Host-Only Ethernet Adapter #4
   Adresse physique . . . . . . . . . . . : 0A-00-27-00-00-04
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::3d1f:3378:ec89:2aa0%4(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.4.2.1(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . :
   IAID DHCPv6 . . . . . . . . . . . : 990511143
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-25-3D-21-5D-00-6F-00-00-32-5E
   Serveurs DNS. . .  . . . . . . . . . . : fec0:0:0:ffff::1%1
                                       fec0:0:0:ffff::2%1
                                       fec0:0:0:ffff::3%1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

Carte réseau sans fil Connexion au réseau local* 9 :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Microsoft Wi-Fi Direct Virtual Adapter
   Adresse physique . . . . . . . . . . . : 08-71-90-3F-31-A9
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui

Carte réseau sans fil Connexion au réseau local* 10 :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Microsoft Wi-Fi Direct Virtual Adapter #2
   Adresse physique . . . . . . . . . . . : 0A-71-90-3F-31-A8
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui

Carte Ethernet VMware Network Adapter VMnet1 :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : VMware Virtual Ethernet Adapter for VMnet1
   Adresse physique . . . . . . . . . . . : 00-50-56-C0-00-01
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::547d:2060:c48d:cf6a%7(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.209.1(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Bail obtenu. . . . . . . . . . . . . . : mercredi 29 avril 2020 08:54:22
   Bail expirant. . . . . . . . . . . . . : mercredi 29 avril 2020 10:55:13
   Passerelle par défaut. . . . . . . . . :
   Serveur DHCP . . . . . . . . . . . . . : 192.168.209.254
   IAID DHCPv6 . . . . . . . . . . . : 620777558
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-25-3D-21-5D-00-6F-00-00-32-5E
   Serveurs DNS. . .  . . . . . . . . . . : fec0:0:0:ffff::1%1
                                       fec0:0:0:ffff::2%1
                                       fec0:0:0:ffff::3%1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

Carte Ethernet VMware Network Adapter VMnet8 :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : VMware Virtual Ethernet Adapter for VMnet8
   Adresse physique . . . . . . . . . . . : 00-50-56-C0-00-08
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::e110:8c59:a443:b505%2(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.30.1(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Bail obtenu. . . . . . . . . . . . . . : mercredi 29 avril 2020 08:54:22
   Bail expirant. . . . . . . . . . . . . : mercredi 29 avril 2020 10:55:13
   Passerelle par défaut. . . . . . . . . :
   Serveur DHCP . . . . . . . . . . . . . : 192.168.30.254
   IAID DHCPv6 . . . . . . . . . . . : 637554774
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-25-3D-21-5D-00-6F-00-00-32-5E
   Serveurs DNS. . .  . . . . . . . . . . : fec0:0:0:ffff::1%1
                                       fec0:0:0:ffff::2%1
                                       fec0:0:0:ffff::3%1
   Serveur WINS principal . . . . . . . . : 192.168.30.2
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : home
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
   Adresse physique . . . . . . . . . . . : 08-71-90-3F-31-A8
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::f413:e9c0:26f6:5211%11(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.42(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Bail obtenu. . . . . . . . . . . . . . : samedi 25 avril 2020 16:21:36
   Bail expirant. . . . . . . . . . . . . : jeudi 30 avril 2020 08:54:22
   Passerelle par défaut. . . . . . . . . : 192.168.1.1
   Serveur DHCP . . . . . . . . . . . . . : 192.168.1.1
   IAID DHCPv6 . . . . . . . . . . . : 201879952
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-25-3D-21-5D-00-6F-00-00-32-5E
   Serveurs DNS. . .  . . . . . . . . . . : 192.168.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

Carte Ethernet Connexion réseau Bluetooth :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Bluetooth Device (Personal Area Network)
   Adresse physique . . . . . . . . . . . : 08-71-90-3F-31-AC
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
```

Les "Carte Ethernet VirtualBox Host-Only Network" correspondent aux cartes réseau de chacunes de mes VM sur VirtualBox.
Les "Carte Ethernet VMware Network Adapter VMnet" correspondent aux cartes réseau de chacunes de mes VM sur VMWare.
Les "Carte réseau sans fil Connexion au réseau local" servent à avoir accès au partage de connexion.
La "Carte réseau sans fil Wi-Fi" correspond à la carte réseau qui me permet d'avoir accès à une connexion sans-fil WIFI.
La "Carte Ethernet Connexion réseau Bluetooth" sert à avoir accès au service Bluetooth.

Liste de tous les ports TCP et UDP en utilistation 

```
PS C:\Windows\system32> netstat -abo

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:80             LAPTOP-SI7QQC7H:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:135            LAPTOP-SI7QQC7H:0      LISTENING       476
  RpcEptMapper
 [svchost.exe]
  TCP    0.0.0.0:445            LAPTOP-SI7QQC7H:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:5040           LAPTOP-SI7QQC7H:0      LISTENING       8620
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:7680           LAPTOP-SI7QQC7H:0      LISTENING       13208
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49664          LAPTOP-SI7QQC7H:0      LISTENING       840
 [lsass.exe]
  TCP    0.0.0.0:49665          LAPTOP-SI7QQC7H:0      LISTENING       744
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          LAPTOP-SI7QQC7H:0      LISTENING       2524
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49667          LAPTOP-SI7QQC7H:0      LISTENING       2732
  SessionEnv
 [svchost.exe]
  TCP    0.0.0.0:49668          LAPTOP-SI7QQC7H:0      LISTENING       3096
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49669          LAPTOP-SI7QQC7H:0      LISTENING       3632
 [spoolsv.exe]
  TCP    0.0.0.0:49671          LAPTOP-SI7QQC7H:0      LISTENING       816
 Impossible d’obtenir les informations de propriétaire
  TCP    10.3.1.1:139           LAPTOP-SI7QQC7H:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    10.3.2.1:139           LAPTOP-SI7QQC7H:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    10.4.1.1:139           LAPTOP-SI7QQC7H:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    10.4.2.1:139           LAPTOP-SI7QQC7H:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:6463         LAPTOP-SI7QQC7H:0      LISTENING       4712
 [Discord.exe]
  TCP    127.0.0.1:12025        LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12110        LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12119        LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12143        LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12465        LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12563        LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12993        LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12995        LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:27015        LAPTOP-SI7QQC7H:0      LISTENING       14596
 [AppleMobileDeviceProcess.exe]
  TCP    127.0.0.1:27275        LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:50004        LAPTOP-SI7QQC7H:0      LISTENING       7872
 [SocketHeciServer.exe]
  TCP    192.168.1.42:139       LAPTOP-SI7QQC7H:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.42:9543      162.159.135.234:https  ESTABLISHED     13976
 [Discord.exe]
  TCP    192.168.1.42:9550      47:https               ESTABLISHED     13976
 [Discord.exe]
  TCP    192.168.1.42:9594      a2-17-148-11:https     CLOSE_WAIT      3640
 [Video.UI.exe]
  TCP    192.168.1.42:9595      93.184.220.29:http     CLOSE_WAIT      3640
 [Video.UI.exe]
  TCP    192.168.1.42:9600      ams10-011:http         ESTABLISHED     3940
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.42:9838      162.159.138.234:https  ESTABLISHED     13976
 [Discord.exe]
  TCP    192.168.1.42:11670     162.159.133.233:https  ESTABLISHED     13976
 [Discord.exe]
  TCP    192.168.1.42:11710     13.107.21.200:https    TIME_WAIT       0
  TCP    192.168.1.42:11711     13.107.18.11:https     TIME_WAIT       0
  TCP    192.168.1.42:11712     40.101.92.194:https    TIME_WAIT       0
  TCP    192.168.1.42:11715     40.90.23.208:https     ESTABLISHED     7260
  wlidsvc
 [svchost.exe]
  TCP    192.168.1.42:11716     13.107.6.254:https     TIME_WAIT       0
  TCP    192.168.1.42:11717     131.253.33.254:https   TIME_WAIT       0
  TCP    192.168.1.42:11719     204.79.197.222:https   TIME_WAIT       0
  TCP    192.168.1.42:11720     a104-82-75-12:http     TIME_WAIT       0
  TCP    192.168.1.42:11721     13.107.21.200:https    ESTABLISHED     12096
 [SearchUI.exe]
  TCP    192.168.1.42:11722     13.107.21.200:https    ESTABLISHED     12096
 [SearchUI.exe]
  TCP    192.168.1.42:11723     13.107.18.11:https     ESTABLISHED     12096
 [SearchUI.exe]
  TCP    192.168.1.42:11726     13.107.136.254:https   ESTABLISHED     12096
 [SearchUI.exe]
  TCP    192.168.1.42:11727     52.138.220.51:https    TIME_WAIT       0
  TCP    192.168.1.42:11728     server-13-32-128-74:https  ESTABLISHED     12096
 [SearchUI.exe]
  TCP    192.168.1.42:11729     93.184.220.29:http     ESTABLISHED     12096
 [SearchUI.exe]
  TCP    192.168.1.42:11730     20.189.73.146:https    ESTABLISHED     14036
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.42:11731     13.107.3.254:https     ESTABLISHED     12096
 [SearchUI.exe]
  TCP    192.168.1.42:11732     204.79.197.222:https   ESTABLISHED     12096
 [SearchUI.exe]
  TCP    192.168.1.42:49444     40.67.251.132:https    ESTABLISHED     4508
  WpnService
 [svchost.exe]
  TCP    192.168.30.1:139       LAPTOP-SI7QQC7H:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.209.1:139      LAPTOP-SI7QQC7H:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:80                LAPTOP-SI7QQC7H:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:135               LAPTOP-SI7QQC7H:0      LISTENING       476
  RpcEptMapper
 [svchost.exe]
  TCP    [::]:445               LAPTOP-SI7QQC7H:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:7680              LAPTOP-SI7QQC7H:0      LISTENING       13208
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49664             LAPTOP-SI7QQC7H:0      LISTENING       840
 [lsass.exe]
  TCP    [::]:49665             LAPTOP-SI7QQC7H:0      LISTENING       744
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49666             LAPTOP-SI7QQC7H:0      LISTENING       2524
  EventLog
 [svchost.exe]
  TCP    [::]:49667             LAPTOP-SI7QQC7H:0      LISTENING       2732
  SessionEnv
 [svchost.exe]
  TCP    [::]:49668             LAPTOP-SI7QQC7H:0      LISTENING       3096
  Schedule
 [svchost.exe]
  TCP    [::]:49669             LAPTOP-SI7QQC7H:0      LISTENING       3632
 [spoolsv.exe]
  TCP    [::]:49671             LAPTOP-SI7QQC7H:0      LISTENING       816
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:12025            LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:12110            LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:12119            LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:12143            LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:12465            LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:12563            LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:12993            LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:12995            LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:27275            LAPTOP-SI7QQC7H:0      LISTENING       3940
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:49670            LAPTOP-SI7QQC7H:0      LISTENING       5400
 [jhi_service.exe]
  UDP    0.0.0.0:500            *:*                                    4420
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:4500           *:*                                    4420
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:5050           *:*                                    8620
  CDPSvc
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*                                    1996
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    1996
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    1996
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    1996
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    1996
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    1996
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    1996
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    1996
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    1996
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    1996
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    2844
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*                                    1996
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    1996
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    1996
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    1996
 [chrome.exe]
  UDP    0.0.0.0:5355           *:*                                    2844
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:49728          *:*                                    3940
 Impossible d’obtenir les informations de propriétaire
  UDP    0.0.0.0:55829          *:*                                    4712
 [Discord.exe]
  UDP    0.0.0.0:57813          *:*                                    11224
 [SkypeApp.exe]
  UDP    10.3.1.1:137           *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.3.1.1:138           *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.3.1.1:1900          *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    10.3.1.1:2177          *:*                                    8248
  QWAVE
 [svchost.exe]
  UDP    10.3.1.1:49384         *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    10.3.2.1:137           *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.3.2.1:138           *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.3.2.1:1900          *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    10.3.2.1:2177          *:*                                    8248
  QWAVE
 [svchost.exe]
  UDP    10.3.2.1:49383         *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    10.4.1.1:137           *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.4.1.1:138           *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.4.1.1:1900          *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    10.4.1.1:2177          *:*                                    8248
  QWAVE
 [svchost.exe]
  UDP    10.4.1.1:49385         *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    10.4.2.1:137           *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.4.2.1:138           *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.4.2.1:1900          *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    10.4.2.1:2177          *:*                                    8248
  QWAVE
 [svchost.exe]
  UDP    10.4.2.1:49386         *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:1900         *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:49390        *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:49664        *:*                                    4860
  iphlpsvc
 [svchost.exe]
  UDP    127.0.0.1:49667        *:*                                    14596
 [AppleMobileDeviceProcess.exe]
  UDP    127.0.0.1:49668        *:*                                    14596
 [AppleMobileDeviceProcess.exe]
  UDP    127.0.0.1:49726        *:*                                    3940
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.1.42:137       *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.1.42:138       *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.1.42:1900      *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    192.168.1.42:2177      *:*                                    8248
  QWAVE
 [svchost.exe]
  UDP    192.168.1.42:49389     *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    192.168.30.1:137       *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.30.1:138       *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.30.1:1900      *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    192.168.30.1:2177      *:*                                    8248
  QWAVE
 [svchost.exe]
  UDP    192.168.30.1:49388     *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    192.168.209.1:137      *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.209.1:138      *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.209.1:1900     *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    192.168.209.1:2177     *:*                                    8248
  QWAVE
 [svchost.exe]
  UDP    192.168.209.1:49387    *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    [::]:500               *:*                                    4420
  IKEEXT
 [svchost.exe]
  UDP    [::]:4500              *:*                                    4420
  IKEEXT
 [svchost.exe]
  UDP    [::]:5353              *:*                                    2844
  Dnscache
 [svchost.exe]
  UDP    [::]:5353              *:*                                    1996
 [chrome.exe]
  UDP    [::]:5353              *:*                                    1996
 [chrome.exe]
  UDP    [::]:5353              *:*                                    1996
 [chrome.exe]
  UDP    [::]:5353              *:*                                    1996
 [chrome.exe]
  UDP    [::]:5353              *:*                                    1996
 [chrome.exe]
  UDP    [::]:5353              *:*                                    1996
 [chrome.exe]
  UDP    [::]:5353              *:*                                    1996
 [chrome.exe]
  UDP    [::]:5355              *:*                                    2844
  Dnscache
 [svchost.exe]
  UDP    [::]:57813             *:*                                    11224
 [SkypeApp.exe]
  UDP    [::1]:1900             *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    [::1]:49382            *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::cc2:ed09:3b60:d5c5%18]:1900  *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::cc2:ed09:3b60:d5c5%18]:2177  *:*                                    8248
  QWAVE
 [svchost.exe]
  UDP    [fe80::cc2:ed09:3b60:d5c5%18]:49376  *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::3d1f:3378:ec89:2aa0%4]:1900  *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::3d1f:3378:ec89:2aa0%4]:2177  *:*                                    8248
  QWAVE
 [svchost.exe]
  UDP    [fe80::3d1f:3378:ec89:2aa0%4]:49378  *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::489d:d189:7ecf:9143%19]:1900  *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::489d:d189:7ecf:9143%19]:2177  *:*                                    8248
  QWAVE
 [svchost.exe]
  UDP    [fe80::489d:d189:7ecf:9143%19]:49375  *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::501a:92:ffec:da43%10]:1900  *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::501a:92:ffec:da43%10]:2177  *:*                                    8248
  QWAVE
 [svchost.exe]
  UDP    [fe80::501a:92:ffec:da43%10]:49377  *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::547d:2060:c48d:cf6a%7]:1900  *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::547d:2060:c48d:cf6a%7]:2177  *:*                                    8248
  QWAVE
 [svchost.exe]
  UDP    [fe80::547d:2060:c48d:cf6a%7]:49379  *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::e110:8c59:a443:b505%2]:1900  *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::e110:8c59:a443:b505%2]:2177  *:*                                    8248
  QWAVE
 [svchost.exe]
  UDP    [fe80::e110:8c59:a443:b505%2]:49380  *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::f413:e9c0:26f6:5211%11]:1900  *:*                                    6924
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::f413:e9c0:26f6:5211%11]:2177  *:*                                    8248
  QWAVE
 [svchost.exe]
  UDP    [fe80::f413:e9c0:26f6:5211%11]:49381  *:*                                    6924
  SSDPSRV
 [svchost.exe]
 ```
 
Le processus Windows svchost.exe sert d'hôte pour tous les autres processus et dont le fonctionnement repose sur l'utilisation des DLL.
Il y a ensuite tous les ports utilisé par différentes application comme Discord, Chrome ou Apple Music
 
### User
 
Liste des utilisateurs de la machine
 
 ```
C:\Users\victo>wmic useraccount list full


AccountType=512
Description=Compte d'utilisateur d'administration
Disabled=TRUE
Domain=LAPTOP-SI7QQC7H
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Administrateur
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-87246160-2975926031-1110129299-500
SIDType=1
Status=Degraded


AccountType=512
Description=Compte utilisateur géré par le système.
Disabled=TRUE
Domain=LAPTOP-SI7QQC7H
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=DefaultAccount
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-87246160-2975926031-1110129299-503
SIDType=1
Status=Degraded


AccountType=512
Description=Compte d'utilisateur invité
Disabled=TRUE
Domain=LAPTOP-SI7QQC7H
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Invité
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-87246160-2975926031-1110129299-501
SIDType=1
Status=Degraded


AccountType=512
Description=
Disabled=FALSE
Domain=LAPTOP-SI7QQC7H
FullName=Victor GARCIA
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=victo
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-87246160-2975926031-1110129299-1001
SIDType=1
Status=OK


AccountType=512
Description=Compte d'utilisateur géré et utilisé par le système pour les scénarios Windows Defender Application Guard.
Disabled=TRUE
Domain=LAPTOP-SI7QQC7H
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=WDAGUtilityAccount
PasswordChangeable=TRUE
PasswordExpires=TRUE
PasswordRequired=TRUE
SID=S-1-5-21-87246160-2975926031-1110129299-504
SIDType=1
Status=Degraded
```

Le nom de l'utilisateur qui possède les droits administrateur est "Administrateur"

### Processus

Liste complète des processus en cours : 

```
PS C:\Windows\system32> Get-Service

Status   Name               DisplayName
------   ----               -----------
Stopped  AarSvc_e71b02e     Agent Activation Runtime_e71b02e
Stopped  ACCSvc             ACC Service
Running  AESMService        Intel® SGX AESM
Stopped  AJRouter           Service de routeur AllJoyn
Stopped  ALG                Service de la passerelle de la couc...
Stopped  AppIDSvc           Identité de l’application
Running  Appinfo            Informations d’application
Stopped  AppReadiness       Préparation des applications
Stopped  AppXSvc            Service de déploiement AppX (AppXSVC)
Running  AudioEndpointBu... Générateur de points de terminaison...
Running  Audiosrv           Audio Windows
Stopped  autotimesvc        Heure cellulaire
Running  AVG Antivirus      AVG Antivirus
Running  AVG Firewall       AVG Firewall Service
Running  avgbIDSAgent       avgbIDSAgent
Running  AvgWscReporter     AvgWscReporter
Stopped  AxInstSV           Programme d’installation ActiveX (A...
Stopped  BcastDVRUserSer... Service utilisateur de diffusion et...
Stopped  BDESVC             Service de chiffrement de lecteur B...
Running  BFE                Moteur de filtrage de base
Running  BITS               Service de transfert intelligent en...
Running  BluetoothUserSe... Service de support des utilisateurs...
Running  BrokerInfrastru... Service d’infrastructure des tâches...
Running  BTAGService        Service de passerelle audio Bluetooth
Running  BthAvctpSvc        Service AVCTP
Running  bthserv            Service de prise en charge Bluetooth
Running  camsvc             Service Gestionnaire d’accès aux fo...
Stopped  CaptureService_... CaptureService_e71b02e
Running  cbdhsvc_e71b02e    Service utilisateur du Presse-papie...
Running  CDPSvc             Service de plateforme des appareils...
Running  CDPUserSvc_e71b02e Service pour utilisateur de platefo...
Running  CertPropSvc        Propagation du certificat
Running  CleanupPSvc        AVG TuneUp
Stopped  ClickToRunSvc      Service Microsoft Office « Démarrer...
Running  ClipSVC            Service de licences de client (Clip...
Stopped  COMSysApp          Application système COM+
Stopped  ConsentUxUserSv... ConsentUX_e71b02e
Running  CoreMessagingRe... CoreMessaging
Running  cplspcon           Intel(R) Content Protection HDCP Se...
Stopped  CredentialEnrol... CredentialEnrollmentManagerUserSvc_...
Running  CryptSvc           Services de chiffrement
Running  CxAudioSvc         CxAudioSvc Service
Running  CxUIUSvc           CxUIUSvc Service
Running  CxUtilSvc          CxUtilSvc
Running  DcomLaunch         Lanceur de processus serveur DCOM
Stopped  defragsvc          Optimiser les lecteurs
Stopped  DeviceAssociati... DeviceAssociationBroker_e71b02e
Running  DeviceAssociati... Service d’association de périphérique
Stopped  DeviceInstall      Service d’installation de périphérique
Stopped  DevicePickerUse... DevicePicker_e71b02e
Stopped  DevicesFlowUser... Flux d’appareils_e71b02e
Stopped  DevQueryBroker     Service Broker de découverte en arr...
Running  Dhcp               Client DHCP
Stopped  diagnosticshub.... Service Collecteur standard du conc...
Stopped  diagsvc            Diagnostic Execution Service
Running  DiagTrack          Expériences des utilisateurs connec...
Running  DispBrokerDeskt... Service de stratégie d'affichage
Running  DisplayEnhancem... Service d'amélioration de l'affichage
Stopped  DmEnrollmentSvc    Service d'inscription de la gestion...
Stopped  dmwappushservice   Service de routage de messages Push...
Running  Dnscache           Client DNS
Running  DoSvc              Optimisation de livraison
Stopped  dot3svc            Configuration automatique de réseau...
Running  DPS                Service de stratégie de diagnostic
Stopped  DsmSvc             Gestionnaire d’installation de péri...
Running  DsSvc              Service de partage des données
Running  DusmSvc            Consommation des données
Stopped  Eaphost            Protocole EAP (Extensible Authentic...
Stopped  EFS                Système de fichiers EFS (Encrypting...
Stopped  embeddedmode       Mode incorporé
Stopped  EntAppSvc          Service de gestion des applications...
Running  esifsvc            Intel(R) Dynamic Tuning service
Running  EventLog           Journal d’événements Windows
Running  EventSystem        Système d’événement COM+
Stopped  Fax                Télécopie
Stopped  fdPHost            Hôte du fournisseur de découverte d...
Stopped  FDResPub           Publication des ressources de décou...
Stopped  fhsvc              Service d’historique des fichiers
Running  FontCache          Service de cache de police Windows
Running  FontCache3.0.0.0   Cache de police de Windows Presenta...
Stopped  FrameServer        Serveur de trame de la Caméra Windows
Stopped  GoogleChromeEle... Google Chrome Elevation Service
Stopped  gpsvc              Client de stratégie de groupe
Stopped  GraphicsPerfSvc    GraphicsPerfSvc
Stopped  gupdate            Service Google Update (gupdate)
Stopped  gupdatem           Service Google Update (gupdatem)
Stopped  HfcDisableService  Intel(R) RST HFC Disable Service
Running  hidserv            Service du périphérique d’interface...
Stopped  HvHost             Service d'hôte HV
Stopped  iaStorAfsService   Intel(R) Optane(TM) Memory Service
Stopped  icssvc             Service Point d'accès sans fil mobi...
Running  igfxCUIService2... Intel(R) HD Graphics Control Panel ...
Running  IKEEXT             Modules de génération de clés IKE e...
Running  InstallService     Installation du service Microsoft S...
Running  Intel(R) Capabi... Intel(R) Capability Licensing Servi...
Stopped  Intel(R) TPM Pr... Intel(R) TPM Provisioning Service
Running  IntelAudioService  Intel(R) Audio Service
Running  iphlpsvc           Assistance IP
Stopped  IpxlatCfgSvc       Service de configuration de convers...
Running  jhi_service        Intel(R) Dynamic Application Loader...
Running  KeyIso             Isolation de clé CNG
Stopped  KtmRm              Service KtmRm pour Distributed Tran...
Running  LanmanServer       Serveur
Running  LanmanWorkstation  Station de travail
Running  lfsvc              Service de géolocalisation
Running  LicenseManager     Serveur Gestionnaire de licences Wi...
Stopped  lltdsvc            Mappage de découverte de topologie ...
Running  lmhosts            Assistance NetBIOS sur TCP/IP
Running  LSM                Gestionnaire de session locale
Stopped  LxpSvc             Service d'expérience linguistique
Stopped  MapsBroker         Gestionnaire des cartes téléchargées
Stopped  MessagingServic... MessagingService_e71b02e
Stopped  MozillaMaintenance Mozilla Maintenance Service
Running  mpssvc             Pare-feu Windows Defender
Stopped  MSDTC              Coordinateur de transactions distri...
Stopped  MSiSCSI            Service Initiateur iSCSI de Microsoft
Stopped  msiserver          Windows Installer
Stopped  MsMpiLaunchSvc     MS-MPI Launch Service
Running  MSSQL$YNOV         SQL Server (YNOV)
Running  MSSQLFDLauncher... SQL Full-text Filter Daemon Launche...
Stopped  MySQL80            MySQL80
Stopped  NaturalAuthenti... Authentification naturelle
Stopped  NcaSvc             Assistant Connectivité réseau
Running  NcbService         Service Broker pour les connexions ...
Stopped  NcdAutoSetup       Configuration automatique des périp...
Stopped  Netlogon           Netlogon
Stopped  Netman             Connexions réseau
Running  netprofm           Service Liste des réseaux
Stopped  NetSetupSvc        Service Configuration du réseau
Stopped  NetTcpPortSharing  Service de partage de ports Net.Tcp
Running  NgcCtnrSvc         Conteneur Microsoft Passport
Running  NgcSvc             Microsoft Passport
Running  NlaSvc             Connaissance des emplacements réseau
Running  nsi                Service Interface du magasin réseau
Running  OneSyncSvc_e71b02e Hôte de synchronisation_e71b02e
Stopped  ose64              Office 64 Source Engine
Stopped  p2pimsvc           Gestionnaire d’identité réseau homo...
Stopped  p2psvc             Groupement de mise en réseau de pairs
Running  PcaSvc             Service de l’Assistant Compatibilit...
Stopped  perceptionsimul... Service de simulation de perception...
Stopped  PerfHost           Hôte de DLL de compteur de performance
Running  PhoneSvc           Service téléphonique
Stopped  PimIndexMainten... Données de contacts_e71b02e
Stopped  pla                Journaux & alertes de performance
Running  PlugPlay           Plug-and-Play
Stopped  PNRPAutoReg        Service de publication des noms d’o...
Stopped  PNRPsvc            Protocole PNRP
Running  PolicyAgent        Agent de stratégie IPsec
Running  Power              Alimentation
Stopped  PrintNotify        Extensions et notifications des imp...
Stopped  PrintWorkflowUs... PrintWorkflow_e71b02e
Running  ProfSvc            Service de profil utilisateur
Stopped  PushToInstall      Service PushToInstall de Windows
Stopped  QALSvc             Quick Access Local Service
Stopped  QASvc              Quick Access Service
Running  QWAVE              Expérience audio-vidéo haute qualit...
Stopped  RasAuto            Gestionnaire des connexions automat...
Running  RasMan             Gestionnaire des connexions d’accès...
Stopped  RemoteAccess       Routage et accès distant
Stopped  RemoteRegistry     Registre à distance
Running  ReportServer$YNOV  SQL Server Reporting Services (YNOV)
Stopped  RetailDemo         Service de démo du magasin
Running  RmSvc              Service de gestion radio
Stopped  rpcapd             Remote Packet Capture Protocol v.0 ...
Running  RpcEptMapper       Mappeur de point de terminaison RPC
Stopped  RpcLocator         Localisateur d’appels de procédure ...
Running  RpcSs              Appel de procédure distante (RPC)
Running  RstMwService       Intel(R) Storage Middleware Service
Running  SamSs              Gestionnaire de comptes de sécurité
Stopped  SCardSvr           Carte à puce
Stopped  ScDeviceEnum       Service d’énumération de périphériq...
Running  Schedule           Planificateur de tâches
Stopped  SCPolicySvc        Stratégie de retrait de la carte à ...
Stopped  SDRSVC             Sauvegarde Windows
Stopped  seclogon           Ouverture de session secondaire
Running  SecurityHealthS... Service Sécurité Windows
Running  SEMgrSvc           Gestionnaires des paiements et des ...
Running  SENS               Service de notification d’événement...
Stopped  SensorDataService  Service Données de capteur
Stopped  SensorService      Service de capteur
Stopped  SensrSvc           Service de surveillance des capteurs
Running  SessionEnv         Configuration des services Bureau à...
Running  SgrmBroker         Service Broker du moniteur d'exécut...
Stopped  SharedAccess       Partage de connexion Internet (ICS)
Stopped  SharedRealitySvc   Service de données spatiales
Running  ShellHWDetection   Détection matériel noyau
Stopped  shpamsvc           Shared PC Account Manager
Stopped  smphost            SMP de l’Espace de stockages Microsoft
Stopped  SmsRouter          Service Routeur SMS Microsoft Windows.
Stopped  SNMPTRAP           Interruption SNMP
Stopped  spectrum           Service de perception Windows
Running  Spooler            Spouleur d’impression
Stopped  sppsvc             Protection logicielle
Stopped  SQLAgent$YNOV      SQL Server Agent (YNOV)
Stopped  SQLBrowser         SQL Server Browser
Running  SQLTELEMETRY$YNOV  SQL Server CEIP service (YNOV)
Stopped  SQLWriter          Enregistreur VSS SQL Server
Running  SSDPSRV            Découverte SSDP
Stopped  ssh-agent          OpenSSH Authentication Agent
Running  SstpSvc            Service SSTP (Secure Socket Tunneli...
Running  StateRepository    Service State Repository (StateRepo...
Stopped  Steam Client Se... Steam Client Service
Stopped  stisvc             Acquisition d’image Windows (WIA)
Running  StorSvc            Service de stockage
Stopped  svsvc              Vérificateur de points
Stopped  swprv              Fournisseur de cliché instantané de...
Running  SysMain            SysMain
Running  SystemEventsBroker Service Broker des événements système
Running  TabletInputService Service du clavier tactile et du vo...
Stopped  TapiSrv            Téléphonie
Running  TbtHostControll... Thunderbolt(TM) Application Launcher
Running  TermService        Services Bureau à distance
Running  Themes             Thèmes
Stopped  TieringEngineSe... Gestion des niveaux de stockage
Running  TimeBrokerSvc      Service Broker pour les événements ...
Running  TokenBroker        Gestionnaire de comptes web
Running  TrkWks             Client de suivi de lien distribué
Stopped  TroubleshootingSvc Service de résolution des problèmes...
Stopped  TrustedInstaller   Programme d’installation pour les m...
Stopped  tzautoupdate       Programme de mise à jour automatiqu...
Stopped  UEIPSvc            User Experience Improvement Program
Stopped  UmRdpService       Redirecteur de port du mode utilisa...
Stopped  UnistoreSvc_e71... Stockage des données utilisateur_e7...
Stopped  upnphost           Hôte de périphérique UPnP
Stopped  UserDataSvc_e71... Accès aux données utilisateur_e71b02e
Running  UserManager        Gestionnaire des utilisateurs
Running  UsoSvc             Mettre à jour le service Orchestrator
Stopped  VacSvc             Service de composition audio volumé...
Running  VaultSvc           Gestionnaire d’informations d’ident...
Stopped  VBoxSDS            VirtualBox system service
Running  vds                Disque virtuel
Stopped  VMAuthdService     VMware Authorization Service
Stopped  vmicguestinterface Interface de services d’invité Hyper-V
Stopped  vmicheartbeat      Service Pulsation Microsoft Hyper-V
Stopped  vmickvpexchange    Service Échange de données Microsof...
Stopped  vmicrdv            Service de virtualisation Bureau à ...
Stopped  vmicshutdown       Service Arrêt de l’invité Microsoft...
Stopped  vmictimesync       Service Synchronisation date/heure ...
Stopped  vmicvmsession      Service Hyper-V PowerShell Direct
Stopped  vmicvss            Requête du service VSS Microsoft Hy...
Running  VMnetDHCP          VMware DHCP Service
Stopped  VMUSBArbService    VMware USB Arbitration Service
Running  VMware NAT Service VMware NAT Service
Stopped  VSS                Cliché instantané des volumes
Stopped  W32Time            Temps Windows
Stopped  WaaSMedicSvc       Windows Update Medic Service
Stopped  WalletService      WalletService
Stopped  WarpJITSvc         WarpJITSvc
Stopped  wbengine           Service de moteur de sauvegarde en ...
Running  WbioSrvc           Service de biométrie Windows
Running  Wcmsvc             Gestionnaire des connexions Windows
Stopped  wcncsvc            Windows Connect Now - Registre de c...
Running  WdiServiceHost     Service hôte WDIServiceHost
Running  WdiSystemHost      Hôte système de diagnostics
Stopped  WdNisSvc           Service Inspection du réseau de l’a...
Stopped  WebClient          WebClient
Stopped  Wecsvc             Collecteur d’événements de Windows
Stopped  WEPHOSTSVC         Service hôte du fournisseur de chif...
Stopped  wercplsupport      Prise en charge de l’application Ra...
Stopped  WerSvc             Service de rapport d’erreurs Windows
Stopped  WFDSConMgrSvc      Service Wi-Fi Direct Service de ges...
Stopped  WiaRpc             Événements d’acquisition d’images f...
Stopped  WinDefend          Service antivirus Windows Defender
Running  WinHttpAutoProx... Service de découverte automatique d...
Running  Winmgmt            Infrastructure de gestion Windows
Stopped  WinRM              Gestion à distance de Windows (Gest...
Stopped  wisvc              Service Windows Insider
Running  WlanSvc            Service de configuration automatiqu...
Stopped  wlidsvc            Assistant Connexion avec un compte ...
Stopped  wlpasvc            Service de l’Assistant de profil local
Stopped  WManSvc            Service de gestion de Windows
Stopped  wmiApSrv           Carte de performance WMI
Stopped  WMPNetworkSvc      Service Partage réseau du Lecteur W...
Stopped  workfolderssvc     Dossiers de travail
Running  WpcMonSvc          Contrôle parental
Stopped  WPDBusEnum         Service Énumérateur d’appareil mobile
Running  WpnService         Service du système de notifications...
Running  WpnUserService_... Service utilisateur de notification...
Running  wscsvc             Centre de sécurité
Running  WSearch            Windows Search
Stopped  wuauserv           Windows Update
Stopped  WwanSvc            Service de configuration automatiqu...
Stopped  XblAuthManager     Gestionnaire d'authentification Xbo...
Stopped  XblGameSave        Jeu sauvegardé sur Xbox Live
Stopped  XboxGipSvc         Xbox Accessory Management Service
Stopped  XboxNetApiSvc      Service de mise en réseau Xbox Live
```

## Scripting

Voir repo GIT de ce TP

## Gestion de softs

Le travail du gestionnaire de paquets est de présenter une interface qui aide l'utilisateur à gérer l'ensemble des paquets installés sur son système
Il évite de devoir chercher un installer correct pour chaque logiciel que l'on souhaite installer.
Je pense aussi qu'il doit être une sorte de passerelle entre notre machine et internet, ce qui permet de rester anonyme au moment de l'install.

```
PS C:\Users\victo> choco list --local-only
Chocolatey v0.10.15
chocolatey 0.10.15
1 packages installed.
PS C:\Users\victo> choco list -li
Chocolatey v0.10.15
chocolatey 0.10.15
1 packages installed.

 DriverSetupUtility|1.00.3026
Acer Configuration Manager|2.1.16258
Acer Jumpstart|3.3.19180.60
Active Directory Authentication Library pour SQL Server|14.0.800.90
Adobe Creative Cloud|5.1.0.407
Anaconda3 2019.10 (Python 3.7.4 64-bit)|2019.10
App Explorer|0.273.3.862
AVG Internet Security|20.2.3116
AVG TuneUp|19.1.1209
Brawlhalla|
Browser pour SQL Server 2016|13.1.4001.0
Care Center Service|4.00.3009
Cisco Packet Tracer 7.2.2 64Bit|
Discord|0.0.306
Enregistreur VSS Microsoft pour SQL Server 2016|13.1.4001.0
Fichiers de support d'installation de Microsoft SQL Server 2008|10.3.5500.0
Git version 2.25.0|2.25.0
GNS3|2.2.5
Google Chrome|81.0.4044.138
Kodi|
Microsoft .NET Framework 4.5 Multi-Targeting Pack|4.5.50710
Microsoft .NET Framework 4.5.1 Multi-Targeting Pack|4.5.50932
Microsoft .NET Framework 4.5.1 Multi-Targeting Pack (Français)|4.5.50932
Microsoft .NET Framework 4.5.1 SDK|4.5.51641
Microsoft .NET Framework 4.5.1 SDK (Français)|4.5.51641
Microsoft .NET Framework 4.5.2 Multi-Targeting Pack|4.5.51209
Microsoft .NET Framework 4.5.2 Multi-Targeting Pack (Français)|4.5.51209
Microsoft Help Viewer 1.1|1.1.40219
Microsoft Help Viewer 1.1 Language Pack - FRA|1.1.40219
Microsoft Help Viewer 2.2|2.2.23107
Microsoft MPI (7.0.12437.8)|7.0.12437.8
Microsoft ODBC Driver 13 for SQL Server|14.0.800.90
Microsoft Office 365 ProPlus - fr-fr|16.0.12624.20466
Microsoft OneDrive|20.052.0311.0011
Microsoft SQL Server 2012 Native Client |11.3.6518.0
Microsoft SQL Server 2012 Setup (English)|11.1.3128.0
Microsoft SQL Server 2016 (64-bit)|
Microsoft SQL Server 2016 Setup (English)|13.1.4259.0
Microsoft SQL Server 2016 T-SQL Language Service |13.0.14500.10
Microsoft SQL Server 2017 RC1|
Microsoft SQL Server Data-Tier Application Framework (x86) - fr-FR|14.0.3757.2
Microsoft SQL Server Management Studio - 17.2|14.0.17177.0
Microsoft SQL Server 2014 Management Objects |12.0.2000.8
Microsoft SQL Server 2016 T-SQL ScriptDom|13.1.4001.0
Microsoft System CLR Types pour SQL Server 2017 RC1|14.0.800.90
Microsoft System CLR Types pour SQL Server 2014|12.0.2402.11
Microsoft Teams|1.3.00.12058
Microsoft Visual C++ 2008 Redistributable - x86 9.0.30729.6161|9.0.30729.6161
Microsoft Visual C++ 2010  x64 Redistributable - 10.0.40219|10.0.40219
Microsoft Visual C++ 2010  x86 Redistributable - 10.0.40219|10.0.40219
Microsoft Visual C++ 2012 Redistributable (x64) - 11.0.61030|11.0.61030.0
Microsoft Visual C++ 2012 Redistributable (x86) - 11.0.61030|11.0.61030.0
Microsoft Visual C++ 2013 Redistributable (x64) - 12.0.21005|12.0.21005.1
Microsoft Visual C++ 2013 Redistributable (x64) - 12.0.40660|12.0.40660.0
Microsoft Visual C++ 2013 Redistributable (x86) - 12.0.21005|12.0.21005.1
Microsoft Visual C++ 2013 Redistributable (x86) - 12.0.21005|12.0.21005.1
Microsoft Visual C++ 2013 Redistributable (x86) - 12.0.40660|12.0.40660.0
Microsoft Visual C++ 2015-2019 Redistributable (x64) - 14.24.28127|14.24.28127.4
Microsoft Visual C++ 2015-2019 Redistributable (x86) - 14.25.28508|14.25.28508.3
Microsoft Visual Studio 2010 Tools for Office Runtime (x64)|10.0.50903
Microsoft Visual Studio Code (User)|1.45.1
Module linguistique Microsoft Help Viewer 2.2 - FRA|2.2.23107
Module linguistique Microsoft Visual Studio 2010 Tools pour Office Runtime (x64) - FRA|10.0.50903
Mozilla Firefox 76.0.1 (x64 en-US)|76.0.1
Mozilla Maintenance Service|66.0.5
MySQL Connector C++ 8.0|8.0.19
MySQL Connector J|8.0.19
MySQL Connector Net 8.0.19|8.0.19
MySQL Connector Python v8.0.19|8.0.19
MySQL Connector/ODBC 8.0|8.0.19
MySQL Documents 8.0|8.0.19
MySQL Examples and Samples 8.0|8.0.19
MySQL For Excel 1.3.8|1.3.8
MySQL Installer for Windows - Community|1.4.33.0
MySQL Notifier 1.1.8|1.1.8
MySQL Router 8.0|8.0.19
MySQL Server 8.0|8.0.19
MySQL Shell 8.0.19|8.0.19
MySQL Workbench 8.0 CE|8.0.19
Nmap 7.80|7.80
Norton Security Scan|4.6.1.179
Npcap 0.9983|0.9983
OBS Studio|25.0.1
Oracle VM VirtualBox 6.1.4|6.1.4
Prise en charge linguistique de Microsoft Visual Studio Tools for Applications 2015|14.0.23107.20
Python 3.8.1 (32-bit)|3.8.1150.0
Python Launcher|3.8.6925.0
Quick Access Service|3.00.3008
Service de langage T-SQL Microsoft SQL Server 2017 RC1|14.0.17177.0
Steam|2.10.91.91
Stratégies Microsoft SQL Server 2017 RC1|14.0.800.90
Teams Machine-Wide Installer|1.2.0.34161
Twitch|8.0.0
User Experience Improvement Program Service|4.00.3106
Wallpaper Engine|
WinPcap 4.1.3|4.1.0.2980
WinRAR 5.90 (64-bit)|5.90.0
Wireshark 3.0.6 64-bit|3.0.6
XAMPP|7.4.5-0
99 applications not managed with Chocolatey.

Did you know Pro / Business automatically syncs with Programs and
 Features? Learn more about Package Synchronizer at
 https://chocolatey.org/compare
PS C:\Users\victo>
```
